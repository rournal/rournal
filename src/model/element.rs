use super::rectangle::*;
use super::Number;

pub trait Scalable<T: Number>
{
    fn scale(&mut self, origin: (T,T), x_factor: T, y_factor: T);
}

pub trait Rotatable<T: Number>
{
    fn rotate(&mut self, origin: (T,T), angle: T);
}

pub trait Element<T: Number>
{
    fn origin(&self) -> (T,T);
    fn origin_mut(&mut self) -> (&mut T, &mut T);
    
    fn size(&self) -> (T, T);
}

impl<T: Number> dyn Element<T>
{
    fn translate(&mut self, dx: T, dy: T)
    {
        *self.origin_mut().0 = self.origin().0 + dx;
        *self.origin_mut().1 = self.origin().1 + dy;
    }
    
    fn bounding_rectangle(&self) -> Rect<T>
    {
        Rect::new(self.origin().0, self.origin().1, self.size().0, self.size().1)
    }
    
    fn intersects(&self, area: &Rect<T>) -> bool
    {
        self.bounding_rectangle().intersects(area)
    }

    fn contained(&self, rect: &Rect<T>) -> bool
    {
        rect.contained(&self.bounding_rectangle())
    }
}