extern crate num_traits;

pub mod element;
pub mod line;
pub mod rectangle;
pub mod stroke;

use num_traits::NumAssign;
use std::cmp::Ordering::*;
use std::fmt::Debug;

pub trait Number: NumAssign + PartialOrd + Copy + Debug {}

impl<T> Number for T
where T: NumAssign + PartialOrd + Copy + Debug {}

pub fn min<T: Number>(a : T, b : T) -> T
{ 
    match a.partial_cmp(&b) 
    { 
        Some(Greater) => b,
        None | Some(Less) | Some(Equal) => a
    } 
}

pub fn max<T: Number>(a : T, b : T) -> T
{ 
    match a.partial_cmp(&b) 
    { 
        Some(Less) => b,
        None | Some(Greater) | Some(Equal) => a
    } 
}

pub fn sub_abs<T: Number>(a: T, b: T) -> T
{
    let greater = max(a, b);
    let smaller = min(a, b);
    greater - smaller
}
