use super::rectangle::Rect;
use super::element::*;
use super::{Number,min,max};
use std::iter::*;

pub struct Point<T: Number, P: Number>
{
    pub x: T,
    pub y: T,
    pub pressure: P
}

impl<T: Number, P: Number> Element<T> for Point<T, P>
{
    fn origin(&self) -> (T,T)
    {
        (self.x,self.y)
    }

    fn origin_mut(&mut self) -> (&mut T, &mut T)
    {
        (&mut self.x, &mut self.y)
    }

    fn size(&self) -> (T,T)
    {
        (T::zero(), T::zero())
    }
}

impl<T: Number, P: Number> Rotatable<T> for Point<T, P>
{
    fn rotate(&mut self, origin: (T,T), angle: T)
    {
        
    }
}

impl<T: Number, P: Number> Scalable<T> for Point<T, P>
{
    fn scale(&mut self, origin: (T, T), x_factor: T, y_factor: T)
    {
        let dx = self.x - origin.0;
        let dy = self.y - origin.1;

        self.x = dx * x_factor + origin.0;
        self.y = dy * y_factor + origin.1;
    }
}

pub struct Stroke<T: Number, P: Number>
{
    pub points: Vec<Point<T,P>>
}

impl<T: Number, P: Number> Rotatable<T> for Stroke<T, P>
{
    fn rotate(&mut self, origin: (T,T), angle: T)
    {
        self.points.iter_mut().for_each(|point| point.rotate(origin, angle));
    }
}

impl<T: Number, P: Number> Scalable<T> for Stroke<T, P>
{
    fn scale(&mut self, origin: (T, T), x_factor: T, y_factor: T)
    {
        self.points.iter_mut().for_each(|point| point.scale(origin, x_factor, y_factor));
    }
}