use std::ops::Add;
use super::{min,max,sub_abs};
use super::Number;
use std::iter::Iterator;

pub struct Rect<T: Number>
{
    pub x: T,
    pub y: T,
    width: T,
    height: T
}

impl<T: Number> Rect<T>
{
    pub fn new(x: T, y: T, width: T, height: T) -> Self
    {
        let mut width = width;
        let mut height = height;
        if min(T::zero(), width) == width || min(T::zero(), height) == height
        {
            println!("Attempt to create new Rectangle with negative width/height: width: {:?} height: {:?}, using absolute ...", width, height);
            width = sub_abs(T::zero(), width);
            height = sub_abs(T::zero(), height);
        }
        Self{x, y, width, height}
    }

    pub fn width(&self) -> T
    {
        self.width
    }

    pub fn set_width(&mut self, value: T)
    {
        let mut value = value;
        if min(T::zero(), value) == value
        {
            println!("Attempt to set negative width: width: {:?}, using absolute ...", value);
            value = sub_abs(T::zero(), value);
        }
        self.width = value;
    }

    pub fn height(&self) -> T
    {
        self.height
    }

    pub fn set_height(&mut self, value: T)
    {
        let mut value = value;
        if min(T::zero(), value) == value
        {
            println!("Attempt to set negative height: height: {:?}, using absolute ...", value);
            value = sub_abs(T::zero(), value);
        }
        self.height = value;
    }
}

impl<T: Number> Rect<T>
{
    pub fn intersects(&self, other: &Self) -> bool
    {
        false //TODO
    }

    pub fn contained(&self, other: &Self) -> bool
    {
        false //TODO
    }

    pub fn area(&self) -> T
    {
        self.width * self.height
    }
}

impl<T: Number> Add for Rect<T>
{
    type Output = Self;

    fn add(self, rhs: Self) -> Self
    {
        let min_x = min(self.x, rhs.x);
        let min_y = min(self.y, rhs.y);

        let max_x = max(self.x + self.width, rhs.x + rhs.width);
        let max_y = max(self.y + self.width, rhs.y + rhs.width);

        Self{x: min_x, y: min_y, width: max_x - min_x, height: max_y - min_y}
    }
}