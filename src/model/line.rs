use super::element::Element;
use super::rectangle::Rect;
use super::*;

/// A horizontal line.
pub struct Line<T: Number, P: Number>
{
	pub x0: T,
	pub y0: T,
	pub x1: T,
	pub y1: T,
	/// Pressure value
	pub pressure: P
}

impl<T: Number, P: Number> Line<T, P>
{
	pub fn new(x0: T, y0: T, x1: T, y1: T, pressure: P) -> Self
	{
		// Maybe a check to ensure the origin is top left and x1/y1 have to be >= x0/y0
		Self { x0, y0, x1, y1, pressure }
	}
}

impl<T: Number, P: Number> Element<T> for Line<T, P>
{
    fn origin(&self) -> (T,T)
	{
		(self.x0, self.y0)
	}
	
	fn origin_mut(&mut self) -> (&mut T, &mut T)
	{
		(&mut self.x0, &mut self.y0)
	}
	
	fn size(&self) -> (T,T)
	{
		(sub_abs(self.x1, self.x0), sub_abs(self.y1, self.y0))
	}
}
