use gtk::{prelude::*, Application, ApplicationWindow, Button};
use gio::prelude::*;
use std::env;

mod model;
use model::{line::Line, stroke::Point};

fn main() {
    let application = Application::new(Some("com.github.gtk-rs.examples.basic"), Default::default()).expect("failed to initialize");

    application.connect_activate(|app| {
        let window = ApplicationWindow::new(app);
        window.set_title("First GTK+ Program");
        window.set_default_size(350, 70);

        let button = Button::new_with_label("Click me!");
        button.connect_clicked(|_| {
            println!("Clicked!");
        });
        window.add(&button);

        window.show_all();
    });
    
    //let _line : Line<f32> = Line::new(0f32,0f32,4.56f32,8.9f32,3f32);
    let _point: Point<i32, f32> = Point{x: 1,y: 2, pressure: 3f32};
    
    let args : Vec<String> = env::args().collect();
    application.run(&args);
}